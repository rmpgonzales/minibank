package com.intevalue.minibank.controller;

import java.math.BigInteger;
import java.util.List;

import com.intevalue.minibank.controller.service.UserControllerService;
import com.intevalue.minibank.data.model.BankAccount;
import com.intevalue.minibank.data.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserControllerService userControllerService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<User> list() {
        return userControllerService.getUserList();
    }

    @GetMapping(value = "/account-details", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<BankAccount> accountDetails(@RequestParam(name = "userId", required = true) Integer userId) {
        return userControllerService.getBankAccount(userId);
    }
}
