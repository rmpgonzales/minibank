package com.intevalue.minibank.controller;

import java.util.List;

import com.intevalue.minibank.controller.service.TransactionControllerService;
import com.intevalue.minibank.data.model.TransactionHistory;
import com.intevalue.minibank.data.model.param.TransactionDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/transaction")
public class TransactionController {

    @Autowired
    private TransactionControllerService transactionControllerService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<TransactionHistory> transactionHistoryList(
            @RequestParam(name = "userId", required = false) Integer userId) {
        return transactionControllerService.getTransactionHistory(userId);
    }

    @PutMapping(value = "/submit-transaction", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TransactionHistory transactionHistory(@RequestBody TransactionDetail transactionDetail) {
        transactionControllerService.saveTransaction(transactionDetail);
        return null;
    }
}
