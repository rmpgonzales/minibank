package com.intevalue.minibank.controller.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.intevalue.minibank.constants.Constants;
import com.intevalue.minibank.data.model.TransactionHistory;
import com.intevalue.minibank.data.model.param.TransactionDetail;
import com.intevalue.minibank.data.persistence.BankAccountEntity;
import com.intevalue.minibank.data.persistence.TransactionHistoryEntity;
import com.intevalue.minibank.data.persistence.dao.BankAccountDAO;
import com.intevalue.minibank.data.persistence.dao.TransactionHistoryDAO;
import com.intevalue.minibank.data.persistence.dao.TransactionTypeDAO;
import com.intevalue.minibank.data.persistence.dao.UserDAO;
import com.intevalue.minibank.data.persistence.mapper.TransactionHistoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionControllerService {

    @Autowired
    private TransactionHistoryDAO transactionHistoryDAO;
    @Autowired
    private TransactionHistoryMapper transactionHistoryMapper;

    @Autowired
    private BankAccountDAO bankAccountDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TransactionTypeDAO transactionTypeDAO;

    public List<TransactionHistory> getTransactionHistory(Integer userId) {
        List<TransactionHistory> list = new ArrayList<TransactionHistory>();
        if (Objects.nonNull(userId)) {
            list = transactionHistoryMapper.mapEntityListtoModelList(transactionHistoryDAO.findByUserId(userId));
        } else {
            list = transactionHistoryMapper.mapEntityListtoModelList(transactionHistoryDAO.findAll());
        }
        return list;
    }

    public TransactionHistory saveTransaction(TransactionDetail detail) {
        BankAccountEntity bankAccount = null;
        bankAccount = bankAccountDAO.findByAccountNumber(detail.getAccountNumberDestination());

        // Save bank account details
        if (Objects.nonNull(bankAccount)) {
            if (detail.getTransactionTypeCode().compareToIgnoreCase(Constants.WITHDRAW) == 0) {
                bankAccount.setAmountBalance(bankAccount.getAmountBalance().subtract(detail.getAmount()));
            } else if (detail.getTransactionTypeCode().compareToIgnoreCase(Constants.DEPOSIT) == 0) {
                bankAccount.setAmountBalance(bankAccount.getAmountBalance().add(detail.getAmount()));
            } else if (detail.getTransactionTypeCode().compareToIgnoreCase(Constants.TRANSFER) == 0) {
                BankAccountEntity sourceAccount = bankAccountDAO.findByAccountNumber(detail.getAccountNumberSource());
                if (Objects.nonNull(sourceAccount)) {
                    sourceAccount.setAmountBalance(sourceAccount.getAmountBalance().subtract(detail.getAmount()));
                    bankAccountDAO.save(sourceAccount);
                }
                bankAccount.setAmountBalance(bankAccount.getAmountBalance().add(detail.getAmount()));
            }
            bankAccountDAO.save(bankAccount);
        }
        return transactionHistoryMapper.mapEntitytoModel(saveTransactionHistory(detail));
    }

    private TransactionHistoryEntity saveTransactionHistory(TransactionDetail detail) {
        TransactionHistoryEntity entity = new TransactionHistoryEntity();
        entity.setUser(userDAO.findById(detail.getUserId()).orElse(null));
        entity.setTransactionType(transactionTypeDAO.findById(detail.getTransactionTypeCode()).orElse(null));
        entity.setTransactionDate(LocalDate.now());
        entity.setAccountNumberDestination(detail.getAccountNumberDestination());
        entity.setAmount(detail.getAmount());
        entity.setAccountNumberSource(detail.getAccountNumberSource());
        return transactionHistoryDAO.save(entity);
    }
}
