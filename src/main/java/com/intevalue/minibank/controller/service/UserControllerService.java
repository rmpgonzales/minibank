package com.intevalue.minibank.controller.service;

import java.util.List;

import com.intevalue.minibank.data.model.BankAccount;
import com.intevalue.minibank.data.model.User;
import com.intevalue.minibank.data.persistence.dao.BankAccountDAO;
import com.intevalue.minibank.data.persistence.dao.UserDAO;
import com.intevalue.minibank.data.persistence.mapper.BankAccountMapper;
import com.intevalue.minibank.data.persistence.mapper.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserControllerService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BankAccountMapper bankAccountMapper;
    @Autowired
    private BankAccountDAO bankAccountDAO;

    public List<User> getUserList() {
        return userMapper.mapEntityListtoModelList(userDAO.findAll());
    }

    public List<BankAccount> getBankAccount(Integer userId) {
        return bankAccountMapper.mapEntityListtoModelList(bankAccountDAO.findByUserId(userId));
    }
}
