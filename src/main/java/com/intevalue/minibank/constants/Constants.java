package com.intevalue.minibank.constants;

public class Constants {
    public static String WITHDRAW = "WITHDRAW";
    public static String DEPOSIT = "DEPOSIT";
    public static String TRANSFER = "TRANSFER";
}
