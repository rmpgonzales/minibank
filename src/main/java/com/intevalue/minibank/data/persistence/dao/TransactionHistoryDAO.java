package com.intevalue.minibank.data.persistence.dao;

import java.util.List;

import com.intevalue.minibank.data.persistence.TransactionHistoryEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionHistoryDAO extends JpaRepository<TransactionHistoryEntity, Integer>{
    List<TransactionHistoryEntity> findByUserId(Integer userId);
}
