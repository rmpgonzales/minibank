package com.intevalue.minibank.data.persistence.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.intevalue.minibank.data.model.BankAccount;
import com.intevalue.minibank.data.persistence.BankAccountEntity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class BankAccountMapper {

    @Autowired
    private ModelMapper modelMapper;

    public BankAccount mapEntitytoModel(BankAccountEntity entity) {
        BankAccount model = null;
        if(Objects.nonNull(entity)){
            model = modelMapper.map(entity, BankAccount.class);
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
        }
        return model;
    }

    public List<BankAccount> mapEntityListtoModelList(List<BankAccountEntity> entities){
        List<BankAccount> models = null;
        if(!CollectionUtils.isEmpty(entities)){
            models = entities.stream().map(entity->modelMapper.map(entity, BankAccount.class)).collect(Collectors.toList());
        }
        return models;
    }
}
