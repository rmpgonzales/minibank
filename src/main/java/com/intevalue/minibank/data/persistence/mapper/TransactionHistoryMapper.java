package com.intevalue.minibank.data.persistence.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.intevalue.minibank.data.model.TransactionHistory;
import com.intevalue.minibank.data.persistence.TransactionHistoryEntity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class TransactionHistoryMapper {

    @Autowired
    private ModelMapper modelMapper;

    public TransactionHistory mapEntitytoModel(TransactionHistoryEntity entity) {
        TransactionHistory model = null;
        if(Objects.nonNull(entity)){
            model = modelMapper.map(entity, TransactionHistory.class);
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
        }
        return model;
    }

    public List<TransactionHistory> mapEntityListtoModelList(List<TransactionHistoryEntity> entities){
        List<TransactionHistory> models = null;
        if(!CollectionUtils.isEmpty(entities)){
            models = entities.stream().map(entity->modelMapper.map(entity, TransactionHistory.class)).collect(Collectors.toList());
        }
        return models;
    }    
}
