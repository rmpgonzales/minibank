package com.intevalue.minibank.data.persistence.dao;

import com.intevalue.minibank.data.persistence.TransactionTypeEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionTypeDAO extends JpaRepository<TransactionTypeEntity, String> {

}
