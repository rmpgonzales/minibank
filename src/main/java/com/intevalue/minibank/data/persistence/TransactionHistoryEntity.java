package com.intevalue.minibank.data.persistence;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "transaction_history", schema = "minibank")
public class TransactionHistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_type_code", nullable = false)
    private TransactionTypeEntity transactionType;

    @Column(name = "transactionDate")
    private LocalDate transactionDate;

    @Column(name = "account_number_source")
    private String accountNumberSource;

    @Column(name = "account_number_destination")
    private String accountNumberDestination;

    @Column
    private BigDecimal amount;
}
