package com.intevalue.minibank.data.persistence.dao;

import com.intevalue.minibank.data.persistence.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDAO extends JpaRepository<UserEntity, Integer> {

}
