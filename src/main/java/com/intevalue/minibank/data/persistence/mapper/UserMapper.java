package com.intevalue.minibank.data.persistence.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.intevalue.minibank.data.model.User;
import com.intevalue.minibank.data.persistence.UserEntity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class UserMapper {

    @Autowired
    private ModelMapper modelMapper;

    public User mapEntitytoModel(UserEntity entity) {
        User model = null;
        if(Objects.nonNull(entity)){
            model = modelMapper.map(entity, User.class);
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
        }
        return model;
    }

    public List<User> mapEntityListtoModelList(List<UserEntity> entities){
        List<User> models = null;
        if(!CollectionUtils.isEmpty(entities)){
            models = entities.stream().map(entity->modelMapper.map(entity, User.class)).collect(Collectors.toList());
        }
        return models;
    }
}