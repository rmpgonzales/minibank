package com.intevalue.minibank.data.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import com.intevalue.minibank.data.persistence.BankAccountEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountDAO extends JpaRepository<BankAccountEntity,BigInteger>{
    List<BankAccountEntity> findByUserId(Integer id);   
    BankAccountEntity findByAccountNumber(String accountNumber);
}
