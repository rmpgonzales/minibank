package com.intevalue.minibank.data.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "transaction_type", schema = "minibank")
public class TransactionTypeEntity {
    @Id
    @Column
    private String code;

    @Column
    private String name;
}
