package com.intevalue.minibank.data.model.param;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransactionDetail {
    @JsonProperty
    private Integer userId;

    @JsonProperty
    private String transactionTypeCode;

    @JsonProperty
    private String accountNumberSource;

    @JsonProperty
    private String accountNumberDestination;
    
    @JsonProperty
    private BigDecimal amount;
}
