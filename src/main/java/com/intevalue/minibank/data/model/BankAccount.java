package com.intevalue.minibank.data.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BankAccount {
    @JsonProperty
    private Integer id;

    @JsonProperty
    private Integer userId;

    @JsonProperty
    private String accountNumber;

    @JsonProperty
    private String accountTypeName;

    @JsonProperty
    private BigDecimal amountBalance;
}
