package com.intevalue.minibank.data.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransactionHistory {
    @JsonProperty
    private Integer id;

    @JsonProperty
    private Integer userId;

    @JsonProperty
    private String transactionTypeName;

    @JsonProperty
    private LocalDate transactionDate;

    @JsonProperty
    private String accountNumberSource;

    @JsonProperty
    private String accountNumberDestination;

    @JsonProperty
    private BigDecimal amount;
}
