create table minibank.users
(
    id int primary key auto_increment,
    name varchar(255) not null,
    address varchar(255)
);

create table minibank.account_type
(
    code varchar(20) primary key,
    name varchar(20)
);

create table minibank.bank_account
(
    id int primary key auto_increment,
    user_id int references minibank.users(id),
    account_number varchar(20) not null,
    account_type_code varchar(20) references minibank.account_type(code),
    amount_balance decimal
);

create table minibank.transaction_type
(
    code varchar(20) primary key,
    name varchar(20)
);

create table minibank.transaction_history
(
    id int primary key auto_increment,
    user_id int references minibank.users(id),
    transaction_type_code varchar(20) references minibank.transaction_type(code),
    transaction_date date,
    account_number_source varchar(20),
    account_number_destination varchar(20),
    amount decimal
);