insert into minibank.users
    (name,address)
values
    ('Mark Rosales', 'Pampanga'),
    ('Sarah Mendez', 'Quezon'),
    ('Bobby Cruz', 'Manila');

insert into minibank.account_type
    (code,name)
values
    ('SAVINGS', 'Savings Account'),
    ('CHECKING', 'Checking Account');

insert into minibank.bank_account
    (account_number,amount_balance,account_type_code,user_id)
values
    ('12345-67890', 900000, 'SAVINGS', 1),
    ('09876-54321', 800000, 'CHECKING', 1),
    ('12312-31231', 700000, 'SAVINGS', 2),
    ('45645-64564', 600000, 'CHECKING', 3);

insert into minibank.transaction_type
    (code,name)
values
    ('WITHDRAW', 'Withdraw'),
    ('TRANSFER', 'Transfer'),
    ('DEPOSIT', 'Deposit');

insert into minibank.transaction_history
    (account_number_destination,account_number_source,amount,transaction_date,transaction_type_code,user_id)
values
    ('12345-67890', NULL, 200, '2020-11-08', 'WITHDRAW', 1),
    ('09876-54321', NULL, 3000, '2020-11-09', 'DEPOSIT', 1),
    ('12312-31231', '09876-54321', 5600, '2020-11-08', 'TRANSFER', 2),
    ('45645-64564', NULL, 1200, '2020-11-08', 'DEPOSIT', 3);
